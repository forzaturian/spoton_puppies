# SpotOn_Puppies

**CODING CHALLENGE**
Using the Puppy Adoption site please automate the following: http://puppies.herokuapp.com/ 
Please make these are separate transactions that can run independently of each other using a programming language of your choice.  Please use Page Object Model and Selenium, we should be able to run your script out of the provided directory.
 
**SCENARIOS**
Adopt Brooke, add a Chewy Toy and a Travel Carrier, pay with Check
Adopt Sparky, add a Collar & Leash, pay with Credit Card
Adopt 2 Random Dogs add a Collar & Leash to each, pay with Credit Card
Adopt 2 Random Dogs add a 3 Random Accessories to 1, pay with Credit Card

**SETUP INSTRUCTIONS**
1. Download chomedriver and geckodriver, relevant to browsers versions and indicate the correct path.
2. Run next commands in Terminal:

	`chmod u+x install.sh` 

	`./install.sh`

	`source env/bin/activate`

**TEST RUNNING**
Run the next command in Terminal:
1. any particular test:

	`pytest <test_name>`

2. all tests:

	`pytest`

**PARALLEL RUNNING**
1. 2 browsers at the same time:

	`pytest -n 2`

2. 4 browsers at the same time:

	`pytest -n 4`

**RUN TESTS IN FIREFOX BROWSER**

`pytest --browser firefox`





