from selenium.webdriver.common.by import By


class Locators:
#home page locators
    THANK_YOU_NOTICE = (By.CSS_SELECTOR, '#notice')
    VIEW_DETAILS_BTN = (By.CSS_SELECTOR, '.rounded_button')
    PREVIOUS_PAGE = (By.CSS_SELECTOR, '.previous_page')
    NEXT_PAGE = (By.CSS_SELECTOR, '.next_page')

#details page locators
    ADOPT_ME_BTN = (By.CSS_SELECTOR, '[value="Adopt Me!"]')
    RETURN_LINK = (By.CSS_SELECTOR, 'a[href^="/"]')

#adoption page locators
    ALL_CHECKBOXES = (By.CSS_SELECTOR, '[type="checkbox"]')
    COLLAR = (By.CSS_SELECTOR, '#collar')
    CHEWY_TOY = (By.CSS_SELECTOR, '#toy')
    TRAVEL_CARRIER = (By.CSS_SELECTOR, '#carrier')
    VET_VISIT = (By.CSS_SELECTOR, '#vet')
    COMPLETE_ADOPTION_BTN = (By.CSS_SELECTOR, '[value="Complete the Adoption"]')
    ADOPT_ANOTHER_BTN = (By.CSS_SELECTOR, '[value="Adopt Another Puppy"]')
    CHANGE_MIND_BTN = (By.CSS_SELECTOR, '[value="Change your mind"]')

#payment page locators
    NAME_FIELD = (By.CSS_SELECTOR, '#order_name')
    ADDRESS_FIELD = (By.CSS_SELECTOR, '#order_address')
    EMAIL_FIELD = (By.CSS_SELECTOR, '#order_email')
    PAYMENT_TYPE = (By.CSS_SELECTOR, '#order_pay_type')
    PLACE_ORDER_BTN = (By.CSS_SELECTOR, '[value="Place Order"]')



