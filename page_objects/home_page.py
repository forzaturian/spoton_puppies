from page_objects.base_page import BasePage
from page_objects.locators import Locators


class HomePage(BasePage):
    def click_view_details(self, n):
        self.click_elements(Locators.VIEW_DETAILS_BTN, n)

    def navigate_to_next_page(self):
        self.single_click(Locators.NEXT_PAGE)

    def complete_payment_message(self):
        message = self.find_element(Locators.THANK_YOU_NOTICE)
        return message


