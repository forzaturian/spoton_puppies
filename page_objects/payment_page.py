from page_objects.base_page import BasePage
from page_objects.locators import Locators
from selenium.webdriver.support.ui import Select


class PaymentPage(BasePage):
    def input_the_name(self, name):
        self.input(Locators.NAME_FIELD, name)

    def input_the_address(self, adr):
        self.input(Locators.ADDRESS_FIELD, adr)

    def input_the_email(self, email):
        self.input(Locators.EMAIL_FIELD, email)

    def select_payment_method(self, type):
        payment = Select(self.find_element(Locators.PAYMENT_TYPE))
        payment.select_by_visible_text(type)

    def place_an_order(self):
        self.single_click(Locators.PLACE_ORDER_BTN)

    def complete_payment(self, name, adr, email, type):
        self.input_the_name(name)
        self.input_the_address(adr)
        self.input_the_email(email)
        self.select_payment_method(type)
        self.place_an_order()
