from random import randint
from page_objects.base_page import BasePage
from page_objects.locators import Locators


class OptionsPage(BasePage):
    def add_chewy_toy(self):
        self.single_click(Locators.CHEWY_TOY)

    def add_travel_carrier(self):
        self.single_click(Locators.TRAVEL_CARRIER)

    def add_collar_and_leash(self):
        self.single_click(Locators.COLLAR)

    def add_3_random_options(self):
        rand_options = self.find_elements(Locators.ALL_CHECKBOXES)
        options = set()
        while len(options) < 3:
            options.add(randint(0, 3))
        for i in options:
            rand_options[i].click()

    def add_multiple_collars(self):
        options_num = len(self.find_elements(Locators.COLLAR))
        collar = self.find_elements(Locators.COLLAR)
        for i in range(0, options_num):
            collar[i].click()

    def adopt_another_dog(self):
        self.single_click(Locators.ADOPT_ANOTHER_BTN)

    def complete_adoption(self):
        self.single_click(Locators.COMPLETE_ADOPTION_BTN)

    def options_for_brooke(self):
        self.add_chewy_toy()
        self.add_travel_carrier()
        self.complete_adoption()

    def options_for_sparky(self):
        self.add_collar_and_leash()
        self.complete_adoption()





