from page_objects.base_page import BasePage
from page_objects.locators import Locators

class DetailsPage(BasePage):
    def click_adopt_button(self):
        self.single_click(Locators.ADOPT_ME_BTN)
