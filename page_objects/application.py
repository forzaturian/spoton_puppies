from page_objects.home_page import HomePage
from page_objects.details_page import DetailsPage
from page_objects.options_page import OptionsPage
from page_objects.payment_page import PaymentPage


class Application:

    def __init__(self, driver):
        self.driver = driver

        self.home = HomePage(self)
        self.details = DetailsPage(self)
        self.options = OptionsPage(self)
        self.payment = PaymentPage(self)
