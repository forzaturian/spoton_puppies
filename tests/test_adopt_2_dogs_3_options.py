from random import randint

def test_adopt_two_dogs(app):
    expected_message = "Thank you for adopting a puppy!"
    num1 = randint(0, 3)
    num2 = randint(0, 3)
    app.home.click_view_details(n=num1)
    app.details.click_adopt_button()
    app.options.adopt_another_dog()
    app.home.navigate_to_next_page()
    app.home.click_view_details(n=num2)
    app.details.click_adopt_button()
    app.options.add_3_random_options()
    app.driver.save_screenshot("options.png")
    app.options.complete_adoption()
    app.payment.complete_payment(name="Charlie Brown",
                                 adr="1800 Washington Ave S, Minneapolis, MN 55454",
                                 email="c.brown@test.com", type="Credit card")
    actual_message = app.home.complete_payment_message().text
    app.driver.save_screenshot("message4.png")
    assert actual_message == expected_message