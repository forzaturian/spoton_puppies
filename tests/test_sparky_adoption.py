def test_brooke_adoption(app):
    expected_message = "Thank you for adopting a puppy!"
    app.home.navigate_to_next_page()
    app.home.click_view_details(n=0)
    app.details.click_adopt_button()
    app.options.options_for_sparky()
    app.payment.complete_payment(name="John Rambo",
                                            adr="1275 E. Business Loop, Bowie, Arizona",
                                            email="john_rambo@test.com", type="Credit card")
    actual_message = app.home.complete_payment_message().text
    app.driver.save_screenshot("message2.png")
    assert actual_message == expected_message
