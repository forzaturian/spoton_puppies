def test_brooke_adoption(app):
    expected_message = "Thank you for adopting a puppy!"
    app.home.click_view_details(n=0)
    app.details.click_adopt_button()
    app.options.options_for_brooke()
    app.payment.complete_payment(name="James Bond",
                                                   adr="30 Wellington Square, Chelsea",
                                                   email="james007@test.com", type="Check")
    actual_message = app.home.complete_payment_message().text
    app.driver.save_screenshot("message1.png")
    assert actual_message == expected_message

