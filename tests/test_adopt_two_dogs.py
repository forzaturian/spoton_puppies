from random import randint

def test_adopt_two_dogs(app):
    expected_message = "Thank you for adopting a puppy!"
    num1 = randint(0, 3)
    num2 = randint(0, 3)
    app.home.click_view_details(n=num1)
    app.driver.save_screenshot("dog1.png")
    app.details.click_adopt_button()
    app.options.adopt_another_dog()
    app.home.navigate_to_next_page()
    app.home.click_view_details(n=num2)
    app.driver.save_screenshot("dog2.png")
    app.details.click_adopt_button()
    app.options.add_multiple_collars()
    app.options.complete_adoption()
    app.payment.complete_payment(name="Scott Turner",
                                 adr="1567 Bartlett Ln, Sacramento, CA 95815",
                                 email="turner_hooch@test.com", type="Credit card")
    actual_message = app.home.complete_payment_message().text
    app.driver.save_screenshot("message3.png")
    assert actual_message == expected_message
