import os
import pytest
from selenium import webdriver
from page_objects.application import Application


BASE_URL = "http://puppies.herokuapp.com/"


def pytest_addoption(parser):
    parser.addoption("--browser", default="chrome")
    parser.addoption("--maximize", action='store_false')
    parser.addoption("--close", action='store_false')


@pytest.fixture
def browser(request):
    browser = request.config.getoption("--browser")
    maximize = request.config.getoption("--maximize")
    close = request.config.getoption("--close")

    if browser == "chrome":
        driver = webdriver.Chrome(executable_path=os.path.expanduser("~/Downloads/drivers/chromedriver"))
    elif browser == "firefox":
        driver = webdriver.Firefox(executable_path=os.path.expanduser("~/Downloads/drivers/geckodriver"))
    else:
        raise ValueError("{} is not supported argument for browser!".format(browser))

    if close:
        request.addfinalizer(driver.quit)

    if maximize:
        driver.maximize_window()

    driver.get(BASE_URL)
    return driver


@pytest.fixture
def app(browser):
    page = Application(browser)
    return page
